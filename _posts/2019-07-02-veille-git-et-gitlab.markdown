---
layout: post
title:  "[Veille] Git & GitLab"
date:   2019-07-02 01:42 -0700
categories: veille
author: Morgane et Élodie
---
<h2 style="text-align: center;">
<span style="font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;"><span style="color: #999999;">-Veille-</span></span><div class="separator" style="clear: both; text-align: center;">
</div>
</h2>
<div style="text-align: center;">
<div class="separator" style="clear: both; text-align: center;">
<a href="https://1.bp.blogspot.com/-6WDxSq2u-7Q/XRtPyFBp83I/AAAAAAAAAAo/mF59-ti5s_QlQ7E32CAZnCq1b8x4GuO7QCLcBGAs/s1600/Capture%2Bd%25E2%2580%2599e%25CC%2581cran%2B2019-07-02%2Ba%25CC%2580%2B14.36.01.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="293" data-original-width="1600" height="58" src="https://1.bp.blogspot.com/-6WDxSq2u-7Q/XRtPyFBp83I/AAAAAAAAAAo/mF59-ti5s_QlQ7E32CAZnCq1b8x4GuO7QCLcBGAs/s320/Capture%2Bd%25E2%2580%2599e%25CC%2581cran%2B2019-07-02%2Ba%25CC%2580%2B14.36.01.png" width="320" /></a></div>
<span style="font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;"><span style="color: #999999;"><br /></span></span></div>
<div>
<h3 style="text-align: center;">
<span style="color: #444444; font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;"><span style="caret-color: rgb(68, 68, 68); color: orange;">Git &amp; GitLab&nbsp;</span></span></h3>
</div>
<div>
<span style="color: #444444; font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;"><span style="caret-color: rgb(68, 68, 68);"><br /></span></span></div>
<div>
<span style="color: #444444; font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;"><span style="caret-color: rgb(68, 68, 68); color: #444444;">Voici un petit résumé de la veille réalisée par Morgane et Elodie.</span></span></div>
<div>
<span style="color: #444444; font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;"><span style="caret-color: rgb(68, 68, 68);"><br /></span></span></div>
<h4 style="text-align: center;">
<span style="color: #444444; font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;"><span style="caret-color: rgb(68, 68, 68); color: orange;"><u>Git</u></span></span></h4>
<div>
<span style="color: #444444; font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;"><span style="caret-color: rgb(68, 68, 68); color: #444444;">Créé par Linus Torvalds (fondateur de Linux), sa première partie apparait le 7 avril 2005.</span></span></div>
<div>
<span style="color: #444444; font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;"><span style="caret-color: rgb(68, 68, 68); color: #444444;"><br /></span></span></div>
<div>
<span style="font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;"><span style="caret-color: rgb(68, 68, 68); color: #444444;"><span style="color: #444444;">Cet outil libre et extraordinaire permet la </span><b><span style="color: #999999;">gestion de versions</span></b><span style="color: #444444;"> en mode SAAS.&nbsp;</span></span></span></div>
<div>
<span style="color: #444444; font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;"><span style="caret-color: rgb(68, 68, 68); color: #444444;"><br /></span></span></div>
<div>
<span style="color: #444444; font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;"><span style="caret-color: rgb(68, 68, 68); color: #444444;">Vous pouvez désormais travailler à plusieurs sur un même projet sans a fusionner "à la main" vos sources.&nbsp;</span></span></div>
<div>
<span style="color: #444444; font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;"><span style="caret-color: rgb(68, 68, 68); color: #444444;"><br /></span></span></div>
<div style="text-align: center;">
<span style="color: #444444; font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;"><span style="caret-color: rgb(68, 68, 68); color: #444444;">Et GitLab alors ? On y arrive.</span></span></div>
<div style="text-align: center;">
<span style="color: #444444; font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;"><span style="caret-color: rgb(68, 68, 68);"><br /></span></span></div>
<h4 style="text-align: center;">
<span style="color: #444444; font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;"><span style="caret-color: rgb(68, 68, 68); color: orange;"><u>GitLab</u></span></span></h4>
<div>
<span style="font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;"><span style="caret-color: rgb(68, 68, 68); color: #444444;"><span style="color: #444444;">GitLab lui permet de<span style="background-color: white;"> </span></span><span style="color: #cccccc;"><b>partager ses productions</b></span><span style="color: #444444;">. Un peu comme un&nbsp;réseau social pour les dev.</span></span></span></div>
<div>
<span style="color: #444444; font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;"><span style="caret-color: rgb(68, 68, 68); color: #444444;"><br /></span></span></div>
<div>
<span style="color: #444444; font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;"><span style="caret-color: rgb(68, 68, 68); color: #444444;">Vous pouvez partager vos codes, vos images, ... Et les mettre à disposition des autres, selon les droits que vous aurez défini préalablement.&nbsp;</span></span></div>
<div>
<span style="color: #444444; font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;"><span style="caret-color: rgb(68, 68, 68); color: #444444;"><br /></span></span></div>
<div>
<span style="color: #444444; font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;">Vous pouvez aussi&nbsp;récupérer des sources d'autres personnes,&nbsp;contrairement à GitHub.</span></div>
<div>
<span style="color: #444444; font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;"><br /></span></div>
<div>
<span style="color: #444444; font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;"><br /></span></div>
<div style="text-align: center;">
<span style="color: #444444; font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;">Pour revoir notre slide, go to :</span></div>
<div style="text-align: center;">
<span style="color: #999999; font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;"><br /></span></div>
<div style="text-align: center;">
https://docs.google.com/presentation/d/1hl_rP_OnlwMm2p7D867oDCOZXVFRmoW29XCVkDqLXww/edit?usp=sharing</div>
<div>
<span style="color: #444444; font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;"><br /></span></div>
<h4 style="text-align: center;">
<span style="color: #444444; font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;">Enjoy !&nbsp;</span></h4>
<div style="text-align: center;">
<span style="color: #444444; font-family: &quot;courier new&quot; , &quot;courier&quot; , monospace;">💪</span></div>
