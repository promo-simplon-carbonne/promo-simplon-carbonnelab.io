---
layout: post
title:  "[Veille] De beaux documents avec du texte"
date:   2019-11-11 23:18 +0200
categories: veilles
author: Timothée
---
De nombreux logiciels possèdent des interfaces graphiques pour réaliser des documents de qualité. Cependant, leurs interfaces peuvent vite devenir très complexes au vu du nombre de fonctionnalités. Il existe des solutions ne reposant pas sur des interfaces graphiques mais sur l'apprentissage d'une syntaxe particulière, qui sera ensuite transformée en d'autres types de documents : .pdf, .html, .png, etc. En voici quelques-unes.

## Documents écrits

### Le markdown

Le markdown est une syntaxe très simple pour rédiger des documents. Il a l'avantage d'être plus simple que le HTML, ce qui le rend idéal pour la prise de notes et la rédaction de billets de blog.

```md
# Titre 1
## Titre 2
*Italique*
**Gras**
[lien](https://example.com)
```
https://markdown-editor.github.io/

### Les avantages
- Syntaxe facile à apprendre et à relire
- Utilisé un peu partout

Alternatives/compléments :
- [LaTeX](https://www.latex-project.org/)
- [AsciiDoc](http://asciidoc.org/index.html)
- [org-mode](https://orgmode.org/fr/index.html)
- [reStructuredText](http://docutils.sourceforge.net/rst.html)
- etc...

## Présentations (slides)

Vous pouvez écrire des présentations en markdown avec [**reveal.js**](https://revealjs.com/#/) !

Alternatives/compléments :
- [Beamer](https://ctan.org/pkg/beamer)
- [Slidy](https://www.w3.org/Talks/Tools/Slidy2/Overview.html#(1))
- [Slideous](https://goessner.net/articles/slideous/)
- [S5](https://meyerweb.com/eric/tools/s5/)
- [DZSlides](http://paulrouget.com/dzslides/)

## Diagrammes / graphiques

PlantUML est un logiciel écrit en Java qui permet de réaliser des diagrammes UML pour la conception logicielle. En plus de diagrammes UML, il gère aussi les diagrammes de Gantt, le maquettage d'interfaces graphiques, le mindmapping, et bien d'autres.

### PlantUML
```plantuml
class Animal {
    espece
    age
    nombre de pattes
    manger()
    crier()
}
```
[Essayer PlantUML](https://www.planttext.com/)  
[Site du projet](http://plantuml.com/fr/)

Alternatives/compléments :
- [mermaidjs](https://mermaidjs.github.io/#/)
- [Chart.js](https://www.chartjs.org/)
- [GoJS](https://gojs.net/latest/index.html)
- [Penrose](http://www.penrose.ink/)

## Todo-list

### Todo.txt

Il est possible de gérer une liste de tâches avec le format Todo.txt. De nombreuses applications sont disponibles pour gérer ce format sur tout type de plateforme : PC, Tablette, Téléphone, etc.

[Todo.txt](http://todotxt.org/)

Alternatives/compléments :
- [org-mode](https://orgmode.org/fr/index.html)
- [Taskwarrior](https://taskwarrior.org/)

## Kanban

### imdone
[imdone](https://imdone.io/)

Alternatives/compléments :
- [Taskell](https://taskell.app/)
- [Cattaz](http://cattaz.io/)
- [readme-kanban-board](https://github.com/entozoon/readme-kanban-board)

## Musique

### Lilypond

LilyPond est un logiciel libre de gravure musicale. Il vous permet de créer des partitions en écrivant du texte. Il possède des environnement d'édition graphiques comme [Frescobaldi](http://www.frescobaldi.org/) ou [Denemo](http://denemo.org/).

[LilyPond](http://lilypond.org/)

Essayer LilyPond : [Lilybin](http://lilybin.com/)

Alternatives/compléments :
- [abc notation](https://abcnotation.com/)
- [alda](https://github.com/alda-lang/alda)
- [chuck](http://chuck.stanford.edu/)
- [text-beat](https://github.com/flipcoder/textbeat)

## Pourquoi le texte brut ?

### Git
Utiliser du texte brut vous permet de gérer tous vos documents avec Git et de faire du versioning !

### Efficacité du clavier
Écrire uniquement du texte permet de se passer de la souris : pratique pour les dactylographes !

### Portabilité
Vos fichiers seront disponibles sur n'importe quelle plate-forme

## Inconvénients
- Nécéssite l'apprentissage d'une syntaxe, ce qui n'est pas aussi intuitif qu'une interface graphique
- Pas toujours l'outil le plus adapté (pour le dessin par exemple)

## Liens

- Pandoc pour convertir des documents : [Pandoc](https://pandoc.org)
- [Plain text project](https://plaintextproject.online/)
- [The Benefits of Using Plain Text](http://www.terminally-incoherent.com/blog/2007/07/18/the-benefits-of-using-plain-text/)
- [The Plain Text Workflow](https://richardlent.github.io/post/the-plain-text-workflow/)
- [Plain text is the best UX](http://derrybirkett.com/plain-text-is-the-best-ux)
