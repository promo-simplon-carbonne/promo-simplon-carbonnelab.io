---
layout: post
title:  "Media Queries"
date:   2019-08-12 11:10 +0200
categories: Veille
author: Audrey
---
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRi1JeF_KNffSqJluQZGCOBzgyMT6w8fcyj1Jb2-dFLQRw1rD84wVE6OZ7eA0dFVYpNBAc39DL9JqRl/embed?start=false&loop=false&delayms=10000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

Media Queries : Se traduit par "requêtes media" en français, il s'agit dans son ensemble d'un module qui va servir à contribuer à la conception de sites webs adaptatifs, c’est à dire des sites dont la consultation va se faire de manière confortable, peu importe le support (ordinateur, tablette, smartphone), ou peu importe la résolution de l’écran.

Les media queries servent à appliquer des styles différents à une page web, modifier les comportements des éléments de la page en fonction du type d’appareil utilisé ou de la résolution de l’écran, pour s’adapter à chaque utilisateur.

Ils sont apparus en même temps que CSS3, mais ce ne sont pas de nouvelles propriétés. Il s’agit de règles à appliquer sous certaines conditions, c’est la règle @media.

Il y a deux façons de les intégrer au css, en chargeant une ou plusieurs feuilles de styles différentes en fonction de la règle utilisée, et ensuite on écrit le code css distinctement dans les différentes fichiers css;

Ou en écrivant la règle directement dans le fichier css habituel.