---
layout: post
title:  "[Veille] FlexBox & CSS Grid"
date:   2019-07-02 04:50 -0700
categories: veille
author: Marveen et Fred
---
<div class="separator" style="clear: both; text-align: center;">
<a href="https://1.bp.blogspot.com/-SRVFiwS-AAo/XRtDStCCQMI/AAAAAAAAE0E/iBQVC-HXAj4JoPDtSbPIUCySiXQ62lINwCLcBGAs/s1600/Veille%2BFlexBox%2B%2526%2BCSS%2BGrid%2BMULLER_GODIO.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="540" data-original-width="960" height="180" src="https://1.bp.blogspot.com/-SRVFiwS-AAo/XRtDStCCQMI/AAAAAAAAE0E/iBQVC-HXAj4JoPDtSbPIUCySiXQ62lINwCLcBGAs/s320/Veille%2BFlexBox%2B%2526%2BCSS%2BGrid%2BMULLER_GODIO.jpg" width="320" /></a></div>
<div class="separator" style="clear: both; text-align: center;">
<br /></div>
<div class="separator" style="clear: both; text-align: center;">
<a href="https://1.bp.blogspot.com/-JgkZweyXPfY/XRtDG0TumPI/AAAAAAAAEzY/cIlZvpfu2NE0dW3FJNzfKINcDVqdTE7jQCLcBGAs/s1600/Veille%2BFlexBox%2B%2526%2BCSS%2BGrid%2BMULLER_GODIO%2B%25281%2529.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="540" data-original-width="960" height="180" src="https://1.bp.blogspot.com/-JgkZweyXPfY/XRtDG0TumPI/AAAAAAAAEzY/cIlZvpfu2NE0dW3FJNzfKINcDVqdTE7jQCLcBGAs/s320/Veille%2BFlexBox%2B%2526%2BCSS%2BGrid%2BMULLER_GODIO%2B%25281%2529.jpg" style="cursor: move;" width="320" /></a></div>
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://1.bp.blogspot.com/-1LMZqngu-6w/XRtDG1vFsSI/AAAAAAAAEzc/UTJvSCim-VYUvyXP4BIMrBHj5v63aExRgCLcBGAs/s1600/Veille%2BFlexBox%2B%2526%2BCSS%2BGrid%2BMULLER_GODIO%2B%25282%2529.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="540" data-original-width="960" height="180" src="https://1.bp.blogspot.com/-1LMZqngu-6w/XRtDG1vFsSI/AAAAAAAAEzc/UTJvSCim-VYUvyXP4BIMrBHj5v63aExRgCLcBGAs/s320/Veille%2BFlexBox%2B%2526%2BCSS%2BGrid%2BMULLER_GODIO%2B%25282%2529.jpg" width="320" /></a></div>
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://1.bp.blogspot.com/-stsOS5IBLtM/XRtDHWN2eDI/AAAAAAAAEzg/tc-im3GPlE0m3rDbpRoJ0NTTBwVPQkK2QCLcBGAs/s1600/Veille%2BFlexBox%2B%2526%2BCSS%2BGrid%2BMULLER_GODIO%2B%25283%2529.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="540" data-original-width="960" height="180" src="https://1.bp.blogspot.com/-stsOS5IBLtM/XRtDHWN2eDI/AAAAAAAAEzg/tc-im3GPlE0m3rDbpRoJ0NTTBwVPQkK2QCLcBGAs/s320/Veille%2BFlexBox%2B%2526%2BCSS%2BGrid%2BMULLER_GODIO%2B%25283%2529.jpg" width="320" /></a></div>
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://1.bp.blogspot.com/-3n2iT6Kq3FA/XRtDH65hvZI/AAAAAAAAEzk/7eQW6je5hhEbY60DPvMBIhAKwIMdxsaQACLcBGAs/s1600/Veille%2BFlexBox%2B%2526%2BCSS%2BGrid%2BMULLER_GODIO%2B%25284%2529.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="540" data-original-width="960" height="180" src="https://1.bp.blogspot.com/-3n2iT6Kq3FA/XRtDH65hvZI/AAAAAAAAEzk/7eQW6je5hhEbY60DPvMBIhAKwIMdxsaQACLcBGAs/s320/Veille%2BFlexBox%2B%2526%2BCSS%2BGrid%2BMULLER_GODIO%2B%25284%2529.jpg" width="320" /></a></div>
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://1.bp.blogspot.com/-hcjiio2Y4Ug/XRtDIS9hKuI/AAAAAAAAEzo/7qnf4adH3tMU04z1ZT6SdRmd8GSYIhmoQCLcBGAs/s1600/Veille%2BFlexBox%2B%2526%2BCSS%2BGrid%2BMULLER_GODIO%2B%25285%2529.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="540" data-original-width="960" height="180" src="https://1.bp.blogspot.com/-hcjiio2Y4Ug/XRtDIS9hKuI/AAAAAAAAEzo/7qnf4adH3tMU04z1ZT6SdRmd8GSYIhmoQCLcBGAs/s320/Veille%2BFlexBox%2B%2526%2BCSS%2BGrid%2BMULLER_GODIO%2B%25285%2529.jpg" width="320" /></a></div>
<div class="separator" style="clear: both; text-align: center;">
<br /></div>
<div class="separator" style="clear: both; text-align: center;">
Lien vers le Codepen:&nbsp;<a href="https://codepen.io/rachelandrew/full/YqqdXL">https://codepen.io/rachelandrew/full/YqqdXL</a></div>
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://1.bp.blogspot.com/-z2LfXbHOCPA/XRtDIdf2P_I/AAAAAAAAEzs/xLGQowZCvDk05OKyazanw-U6NXiJw8xNACLcBGAs/s1600/Veille%2BFlexBox%2B%2526%2BCSS%2BGrid%2BMULLER_GODIO%2B%25286%2529.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="540" data-original-width="960" height="180" src="https://1.bp.blogspot.com/-z2LfXbHOCPA/XRtDIdf2P_I/AAAAAAAAEzs/xLGQowZCvDk05OKyazanw-U6NXiJw8xNACLcBGAs/s320/Veille%2BFlexBox%2B%2526%2BCSS%2BGrid%2BMULLER_GODIO%2B%25286%2529.jpg" width="320" /></a></div>
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://1.bp.blogspot.com/-Xxq_0_k4u-Q/XRtDI58uZ3I/AAAAAAAAEzw/SGsN2pVYZw4jR5L7FV_qrXhnIrIRRjlKgCLcBGAs/s1600/Veille%2BFlexBox%2B%2526%2BCSS%2BGrid%2BMULLER_GODIO%2B%25287%2529.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="540" data-original-width="960" height="180" src="https://1.bp.blogspot.com/-Xxq_0_k4u-Q/XRtDI58uZ3I/AAAAAAAAEzw/SGsN2pVYZw4jR5L7FV_qrXhnIrIRRjlKgCLcBGAs/s320/Veille%2BFlexBox%2B%2526%2BCSS%2BGrid%2BMULLER_GODIO%2B%25287%2529.jpg" width="320" /></a></div>
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://1.bp.blogspot.com/-8CWgtW7EGec/XRtDI6TI2NI/AAAAAAAAEz0/suru_yEnsmwxWBfVjLM0kMqsPaw8i61fgCLcBGAs/s1600/Veille%2BFlexBox%2B%2526%2BCSS%2BGrid%2BMULLER_GODIO%2B%25288%2529.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="540" data-original-width="960" height="180" src="https://1.bp.blogspot.com/-8CWgtW7EGec/XRtDI6TI2NI/AAAAAAAAEz0/suru_yEnsmwxWBfVjLM0kMqsPaw8i61fgCLcBGAs/s320/Veille%2BFlexBox%2B%2526%2BCSS%2BGrid%2BMULLER_GODIO%2B%25288%2529.jpg" width="320" /></a></div>
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://1.bp.blogspot.com/-cS6Zpy8LUtY/XRtDJGsifKI/AAAAAAAAEz4/L-FHb0DFy2A3f_5EZIoGWQE6tyTZANjagCLcBGAs/s1600/Veille%2BFlexBox%2B%2526%2BCSS%2BGrid%2BMULLER_GODIO%2B%25289%2529.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="540" data-original-width="960" height="180" src="https://1.bp.blogspot.com/-cS6Zpy8LUtY/XRtDJGsifKI/AAAAAAAAEz4/L-FHb0DFy2A3f_5EZIoGWQE6tyTZANjagCLcBGAs/s320/Veille%2BFlexBox%2B%2526%2BCSS%2BGrid%2BMULLER_GODIO%2B%25289%2529.jpg" width="320" /></a></div>
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://1.bp.blogspot.com/-dmgIRfjSuNQ/XRtDGur8VaI/AAAAAAAAEzU/3f6WCwK3M4UJw8MfaRF1K75P5WJ_ZBKUACLcBGAs/s1600/Veille%2BFlexBox%2B%2526%2BCSS%2BGrid%2BMULLER_GODIO%2B%252810%2529.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="540" data-original-width="960" height="180" src="https://1.bp.blogspot.com/-dmgIRfjSuNQ/XRtDGur8VaI/AAAAAAAAEzU/3f6WCwK3M4UJw8MfaRF1K75P5WJ_ZBKUACLcBGAs/s320/Veille%2BFlexBox%2B%2526%2BCSS%2BGrid%2BMULLER_GODIO%2B%252810%2529.jpg" width="320" /></a></div>
<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
</div>
<br />
<div class="separator" style="clear: both; text-align: center;">
</div>
<br />
<div class="separator" style="clear: both; text-align: center;">
</div>
<br />
<div class="separator" style="clear: both; text-align: center;">
</div>
<br />