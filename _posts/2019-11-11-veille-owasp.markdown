---
layout: post
title:  "[Veille] OWASP"
date:   2019-11-11 22:24 +0200
categories: veilles
author: Timothée
---
# OWASP

## C'est quoi ?

- OWASP : Open Web Application Security Project
- Organisation à but non lucratif fondée en 2001 par Mark Curphey
- Fournit librement et gratuitement de la documentation, des outils, des articles, de la recherche, des ressources en rapport avec la sécurité des applications web
- Leur projet le plus connu est sans doute l'OWASP Top Ten

## OWASP Top Ten

Le top 10 OWASP est une liste des 10 vulnérabilités les plus critiques dans les applications web. Cette liste est régulièrement mise à jour et la dernière version date de 2017.

## 1. Les injections

L'injection est une attaque visant à envoyer du code malveillant à un interpréteur qui va l'exécuter.
Un attaquant peut par exemple exploiter le formulaire d'une page web pour envoyer des instructions SQL et récupérer des données dans une base de données : c'est ce qu'on appelle une injection SQL.
Pour se prémunir des injections, il est nécessaire de filtrer les données envoyées par les utilisateurs pour s'assurer qu'elles ne sont pas dangereuses.

## 2. Authentification de mauvaise qualité

Cette vulnérabilité concerne les écrans de connexion. Elle donne à un attaquant accès à des comptes utilisateurs ou même des comptes administrateurs. Certaines faiblesses dans un système d'authentification peuvent rendre les applications particulièrement vulnérables à ce genre d'attaques :
- Pas de protection contre la force brute
- Autorisation des mots de passe faibles ou par défaut comme "1234" ou "admin"
- Pas de chiffrement du mot de passe
- Pas d'authentification à deux facteurs

## 3. Exposition de données sensibles

Certaines données, comme les données personnelles ou les infos bancaires/médicales peuvent être récupérées par un attaquant. Plusieurs facteurs peuvent rendre vulnérable à ce type d'attaque :
- Les données circulent en clair (utilisation de HTTP au lieu de HTTPS par exemple)
- Les algorithmes de chiffrement des données utilisés sont trop faibles
- Les données sensibles sont stockées alors que ce n'est pas nécessaire

## 4. XML Entités externes

Les fichiers XML doivent passer par une étape appelée "Parsing" ou "Analyse Syntaxique" pour être compris par un ordinateur. Cette étape est gérée par un processeur XML. Le problème, c'est que ce programme peut être vulnérable et permettre à un attaquant d'insérer du contenu hostile dans un document XML.
Pour s'en prémunir, il faut au moins s'assurer que les processeurs XML sont à jour, et si possible se passer de XML en utilisant un format de données moins complexe comme JSON. 

## 5. Manque dans le contrôle de l'accès

Les contrôles d'accès s'assurent des droits et permissions de chaque utilisateur. Par exemple, l'administrateur d'un site peut accéder à des pages qu'un utilisateur normal ne doit pas voir. Un site mal sécurisé peut permettre à un attaquant d'accéder à des données potentiellement sensibles.
Pour éviter ce genre d'attaque, on peut par exemple :
- Tout bloquer par défaut et ne laisser un accès libre qu'à des ressources publiques
- Désactiver le listing des dossiers du serveur
- Configurer des jetons d'autorisation

## 6. Mauvaise configuration de sécurité

Les attaquants peuvent tenter d'exploiter des vulnérabilités non corrigées, des pages inutilisées, des répertoires non protégés pour se faire une idée du fonctionnement du système qu'ils attaquent. Pour s'en prémunir on peut désactiver toute fonctionnalité inutile et se débarrasser de toute dépendance dont on n'a plus besoin et changer les paramètres par défaut.

## 7. Cross-Site Scripting

Le Cross-Site Scripting permet d'exécuter du code malveillant sur un site légitime. Ce code est souvent ajouté à la fin d'une url ou posté sur une page qui affiche du contenu généré par les utilisateurs. Ce code s'exécute côté client et peut servir à récupérer des données sensibles comme les cookies, les données de géolocalisation ou les données de la webcam.
Pour éviter le XSS :
- On évitera de permettre aux utilisateurs de soumettre directement du code HTML
- On protégera l'accès aux cookies en vérifiant l'adresse IP

## 8. Désérialisation non sécurisée

La sérialisation, c'est le fait de convertir une donnée en une suite d'informations plus petites pour la sauvegarder plus facilement ou la transporter sur le réseau. La désérialisation est le processus inverse.
Pour se prémunir des attaques, il ne faut pas accepter les objets sérialisés provenant de sources non sûres.

## 9. Utilisation de Composants avec des Vulnérabilités Connues

Beaucoup de projets utilisent des composants comme des frameworks ou des librairies. Ces dépendances peuvent amener des failles de sécurité, surtout si le logiciel est obsolète. Étant donné que certains de ces composants sont très largement utilisés, ils sont une cible de choix pour les attaquants.
Il faut régulièrement mettre à jour les composants qu'on utilise et se débarrasser de ceux qu'on n'utilise plus.

## 10. Supervision et Journalisation Insuffisantes

Le fait de ne pas surveiller correctement un système d'informations peut grandement réduire sa sécurité. Il faut faire en sorte de correctement enregistrer les traces d'activité et les erreurs pour détecter au plus tôt les attaques sur une application.

## Sources :

- [https://en.wikipedia.org/wiki/OWASP](https://en.wikipedia.org/wiki/OWASP)
- [https://www.youtube.com/watch?v=RGA5kLO9vjs](https://www.youtube.com/watch?v=RGA5kLO9vjs)
- [https://www.cloudflare.com/learning/security/threats/owasp-top-10/](https://www.cloudflare.com/learning/security/threats/owasp-top-10/)
- [https://github.com/OWASP/Top10/tree/master/2017/](https://github.com/OWASP/Top10/tree/master/2017/)
- [https://fr.wikipedia.org/wiki/Sérialisation](https://fr.wikipedia.org/wiki/Sérialisation)
