---
layout: post
title:  "Salut ! Moi c'est Vincent !"
date:   2019-07-23 13:40 +0200
categories: présentations
author: Vincent
---
Bienvenue !
--------

Bonjour à toi, explorateur du  Web !

Si tu lis ces lignes..eh bien déjà, merci à toi ! Je vais me présenter un peu, que tu saches qui s'adresse à toi.

Donc, je m'appelle **Vincent**, j'ai *28* ans et je suis actuellement en formation dans l'organisme ***Simplon Occitanie*** pour devenir *"Développeur Web / Web Mobile"*

Je pourrais vous en dire encore bien plus sur moi mais pour le moment, je vais m'en tenir à ça (parce qu'en fait, c'est un billet de test pour mettre en ligne un billet sur le blog de la promo)

A bientôt !